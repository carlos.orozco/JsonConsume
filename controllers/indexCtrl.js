'use strict';

function indexCtrl() {
	const doc = document;
	const callbackPerson = function (response){
		let html ="";
		response = response.data

		for (var i=0; i < response.length; i++) {
			html += `<div class="card-person">
						<div class="content-card-person">
							<img src="${response[i].img}">
						</div>
						<div class="content-details" data-id="${response[i].id}">
							<p>${response[i].description}</p>
							<a href="">Ver Mas</a>
						</div>
					</div>`
		}
		doc.getElementById('loadPerson').innerHTML = html;
	}

	axios.get('data/person.json').then(callbackPerson)
}


indexCtrl()